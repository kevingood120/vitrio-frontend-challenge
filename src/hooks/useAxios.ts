import axios from 'axios'

const useAxios = () => {
    
    const axiosRef = axios.create({
        baseURL: 'https://bitbucket.org/vitriosdev/vitrio-dev-test/raw/master/data/'
    }) 
    
    return axiosRef
}

export {
    useAxios
}